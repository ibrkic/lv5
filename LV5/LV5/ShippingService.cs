﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5
{
    class ShippingService
    {
        public ShippingService() { }
        public double DeliveryPrice(Box box,double pricePerKg)
        {
            double deliveryPrice = 0;
            deliveryPrice = pricePerKg * box.Weight;
            return deliveryPrice;
        }
    }
}
