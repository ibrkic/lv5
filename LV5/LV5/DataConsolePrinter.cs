﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5
{
    class DataConsolePrinter
    {
        public void ConsolePrinter(IDataset dataset)
        {
            IReadOnlyCollection<List<string>> data = dataset.GetData();

            if (data == null)
            {
                Console.WriteLine("Access not granted");
                return;
            }

            foreach (List<string> row in data)
            {
                Console.WriteLine();
                foreach (string column in row)
                {
                    Console.Write(column + " ");


                }
                Console.Write("\n");

            }

        }
    }
}
