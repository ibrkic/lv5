﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5
{
    class Program
    {
        static void Main(string[] args)
        {
            IDataset dataset = new VirtualProxyDataset(@"text.csv");
            DataConsolePrinter printer = new DataConsolePrinter();
            printer.ConsolePrinter(dataset);
            User user = User.GenerateUser("UserName");
            IDataset dataset2 = new ProtectionProxyDataset(user);
            printer.ConsolePrinter(dataset2);
            User user2 = User.GenerateUser("UserName2");
            IDataset dataset3 = new ProtectionProxyDataset(user2);
            printer.ConsolePrinter(dataset3);
        }
    }
}
